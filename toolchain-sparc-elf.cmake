include(CMakeForceCompiler)

SET(CMAKE_SYSTEM_NAME SPARC-ELF)

# The environment variables should be already set
# in order to do proper cross compiling
CMAKE_FORCE_C_COMPILER(sparc-elf-gcc GNU)
CMAKE_FORCE_CXX_COMPILER(sparc-elf-g++ GNU)

set (CMAKE_C_FLAGS $ENV{CFLAGS}) 
set (CMAKE_SHARED_LIBRARY_LINK_C_FLAGS $ENV{LDFLAGS})

add_definitions(-std=c99)

set (CMAKE_AR sparc-elf-ar)
set (CMAKE_RANLIB sparc-elf-ranlib)

SET(CMAKE_C_ARCHIVE_CREATE "<CMAKE_AR> <LINK_FLAGS> cr <TARGET> <OBJECTS>")
SET(CMAKE_C_ARCHIVE_FINISH "<CMAKE_RANLIB> <TARGET>")

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE NEVER)

set(CMAKE_BUILD_TYPE Release)

