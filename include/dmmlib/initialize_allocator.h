/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file 	initialize_allocator.h
 * \author 	Ioannis Koutras (joko@microlab.ntua.gr)
 * \date 	September, 2011
 * \brief 	dmmlib allocator initializator.
 */

#ifndef INITIALIZE_ALLOCATOR_H
#define INITIALIZE_ALLOCATOR_H

#include <dmmlib/heap.h>
#include "dmm_config.h"

#ifdef NO_SYSTEM_CALLS
/**
 * Initialize an allocator. Since no system calls can be made, the developer
 * has to define explicitly the memory space the allocator will manage.
 *
 * \param allocator The address of the allocator.
 * \param starting_address The starting addres of the memory space which the
 * allocator has to handle.
 * \param size The total size of the memory space which the allocator has to
 * handle.
 */
void initialize_allocator(allocator_t *allocator, void *starting_address,
  size_t size);
#else /* NO_SYSTEM_CALLS */
/**
 * Initialize an allocator.
 *
 * \param allocator The address of the allocator.
 */
void initialize_allocator(allocator_t *allocator);
#endif /* NO_SYSTEM_CALLS */

#endif /* INITIALIZE_ALLOCATOR_H */
