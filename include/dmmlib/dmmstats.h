/*
 *   Copyright 2012 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   dmmstats.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Statistics data structure.
 */

#ifndef DMMSTATS_H
#define DMMSTATS_H

#include <stddef.h> /* for size_t */
#include <inttypes.h>

/** Statistics data structure. */
typedef struct dmmstats_s {
#ifdef REQUEST_SIZE_INFO
    size_t total_mem_requested; /**< Total memory actively used by the
                                  application. */
#endif /* REQUEST_SIZE_INFO */
    size_t total_mem_allocated; /**< Total memory allocated by the 
                                  application. */
    uint32_t live_objects; /**< Number of the currently used blocks. */
    uint32_t num_malloc; /**< Number of malloc()'s served. */
    uint32_t num_free; /**< Number of free()'s served. */
    uint32_t num_realloc; /**< Number of realloc()'s served. */
} dmmstats_t;

#endif /* DMMSTATS_H */
