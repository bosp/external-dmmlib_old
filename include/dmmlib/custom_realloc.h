/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/**
 * \file    custom_realloc.h
 * \author  Ioannis Koutras (joko@microlab.ntua.gr)
 * \date    November, 2011
 * \brief   Function prototype of realloc for the dmmlib allocator.
 */

#ifndef CUSTOM_REALLOC_H
#define CUSTOM_REALLOC_H
#include "dmm_config.h"
#include <dmmlib/dmmlib.h>
#include <dmmlib/pool.h>

/**
 * Try to change the size of an allocation on a specific allocator and heap
 *
 * \param allocator 	The pointer to the allocator who manages the block.
 * \param raw_block     The pointer to the raw block who manages the block.
 * \param ptr 		The pointer to the data part of the original memory block.
 * \param size 		The new desired size of the block.
 *
 * \return 		The pointer to the data part of the memory block which
 * has the new, desired size.
 */ 
void * custom_ahrealloc(allocator_t *allocator, raw_block_header_t *raw_block, void *ptr, size_t size);

#endif /* CUSTOM_REALLOC_H */
