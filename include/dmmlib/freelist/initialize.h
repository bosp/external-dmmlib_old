/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#ifndef FL_INITIALIZE_H
#define FL_INITIALIZE_H

#include <stddef.h>

/** Initializes the free-list organization inside a raw block.
 *
 * @param address        The address of the free-list metadata
 * @param available_size The available size of the raw block for free-list
 *                       metadata and memory blocks
 */
void initialize_freelist(void *address, size_t available_size);

#endif /* FL_INITIALIZE_H */
