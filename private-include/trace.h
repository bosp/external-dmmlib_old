/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   trace.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Tracing functions
 */

#ifndef TRACE_H
#define TRACE_H
#include "dmm_config.h"

#if TRACE_LEVEL >= 1

#include <stdio.h>

/** Function for Level 1 trace messages */
#define TRACE_1(...) fprintf(stderr, __VA_ARGS__)
#else /* TRACE_LEVEL >= 1 */
/** Function for Level 1 trace messages */
#define TRACE_1(...) /* do nothing */
#endif /* TRACE_LEVEL >= 1 */

#if TRACE_LEVEL >= 2
/** Function for Level 2 trace messages */
#define TRACE_2(...) fprintf(stderr, __VA_ARGS__)
#else /* TRACE_LEVEL >= 2 */
/** Function for Level 2 trace messages */
#define TRACE_2(...) /* do nothing */
#endif /* TRACE_LEVEL >= 2 */

#if TRACE_LEVEL >= 3
/** Function for Level 3 trace messages */
#define TRACE_3(...) fprintf(stderr, __VA_ARGS__)
#else /* TRACE_LEVEL >= 3 */
/** Function for Level 3 trace messages */
#define TRACE_3(...) /* do nothing */
#endif /* TRACE_LEVEL >= 3 */

#endif /* TRACE_H */
