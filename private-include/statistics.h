/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   statistics.h
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Statistics function.
 */

#ifndef STATISTICS_H
#define STATISTICS_H
#include "dmm_config.h"

#include <inttypes.h>
#include "dmmlib/dmmstats.h"

/** DMM event type enumeration */
typedef enum dmm_event_type_en {
    MALLOC,
    FREE
} dmm_event_type;

void update_stats
(
 dmmstats_t *dmm_stats,
 dmm_event_type event,
#ifdef REQUEST_SIZE_INFO
 size_t mem_req,
#endif /* REQUEST_SIZE_INFO */
 size_t mem_alloc
);

#endif /* STATISTICS_H */
