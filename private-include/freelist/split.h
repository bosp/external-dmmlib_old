/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#ifndef SPLIT_H
#define SPLIT_H
#include "dmm_config.h"

#include "dmmlib/freelist/freelist_rb.h"
#include "dmmlib/freelist/block_header.h"

/**
 * Splits a memory block to two blocks: one with the requested size and the
 * another one with the remaining space.
 *
 * @param allocator The involved allocator.
 * @param heap The heap of the memory block.
 * @param ptr The memory block to be split.
 * @param req_size The size which the first block will use.
 */ 
void split(freelist_rb_t *raw_block, block_header_t *ptr, size_t req_size);

#endif /* SPLIT_H */
