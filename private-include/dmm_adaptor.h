/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#ifndef DMM_ADAPTOR_H
#define DMM_ADAPTOR_H

#include <dmmlib/heap.h>

void set_frag_params(heap_t *heap);
void set_foot_params(heap_t *heap);

void update_frag_params(heap_t *heap);
void update_foot_params(heap_t *heap);

float get_current_fragmentation(heap_t *heap);
void check_footprint(heap_t *heap);
void malloc_state_refresh(heap_t *heap);
void free_state_refresh(heap_t *heap);

#endif /* DMM_ADAPTOR_H */
