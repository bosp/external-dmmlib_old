#!/usr/bin/env python

import argparse
import re
import random
import string

def parse_memory_trace(input_file, output_file):
    active_memory_requests = {}

    malloc_pattern = re.compile(r'^dmmlib - m (0x\w+) (\d+)')
    free_pattern   = re.compile(r'^dmmlib - f (0x\w+)')

    for line in input_file:
        malloc_match = malloc_pattern.match(line)
        if malloc_match:
            if malloc_match.group(1) not in active_memory_requests:
                active_memory_requests[malloc_match.group(1)] = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(6))
                variable_declaration = ''.join(['    void *var_', active_memory_requests[malloc_match.group(1)], ';\n'])
                output_file.write(variable_declaration)
            new_line = ''.join(['    var_', active_memory_requests[malloc_match.group(1)], ' = malloc(', malloc_match.group(2), ');\n'])
            output_file.write(new_line)
        free_match = free_pattern.match(line)
        if free_match:
            new_line = ''.join(['    free(var_', active_memory_requests[free_match.group(1)], ');\n'])
            del active_memory_requests[free_match.group(1)]
            output_file.write(new_line)

    input_file.close()

def main():
    parser = argparse.ArgumentParser(
        description='''Generate a C file of dynamic memory operations from a dmmlib
            trace''')
    parser.add_argument('tracefile', type=argparse.FileType('rt'),
        help='dmmlib trace from the application')
    parser.add_argument('-o', '--output', metavar='outputfile', type=argparse.FileType('wt'),
        help='generated C file, will use output.c if not available')

    try:
        args = parser.parse_args()
    except IOError, msg:
        parser.error(str(msg))

    if args.output is None:
        args.output = open('output.c', 'wt')

    template_file = open('benchmark_template.c', 'rt')
    for line in template_file:
        if line == '$instructions\n':
            parse_memory_trace(args.tracefile, args.output)
        else:
            args.output.write(line)

    args.output.close()

if __name__ == "__main__":
    main()
