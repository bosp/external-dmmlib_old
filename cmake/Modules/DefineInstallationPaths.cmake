SET(INCLUDE_INSTALL_DIR
   "${CMAKE_INSTALL_PREFIX}/include"
   CACHE PATH "The subdirectory to the header prefix (default prefix/include)"
)

SET(LIB_INSTALL_DIR
  "${CMAKE_INSTALL_PREFIX}/lib"
  CACHE PATH "The subdirectory relative to the install prefix where libraries will be installed (default is prefix/lib)"
)
