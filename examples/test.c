#include <stdio.h>
#include <stdlib.h>
#include <dmmlib/dmmlib.h>

int main(void) {
    void *p1, *p2, *p3;

    p1 = malloc((size_t) 1024);
    free(p1);
    p2 = malloc((size_t) 512);
    p3 = malloc((size_t) 394);
    free(p2);
    free(p3);

    return 0;
}
