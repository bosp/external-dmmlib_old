# NTUA / ICCS dmmlib

Distributed within the context of the [2PARMA project](http://www.2parma.eu).

## Requirements

* cmake v2.6 or higher
* GCC compiler v3.4 or higher

## Usage

1. Create the build directory, e.g. _mkdir build_
2. Go to the build directory, e.g. _cd build_
3. Do cmake {DMMLIB Source Directory} -DLINUXTEST=ON , e.g. _cmake .. -DLINUXTEST=ON_
4. _make_

## Linking with applications

There are currently two options:

* Link the library to your application  
_gcc -I{DMMLIB Source Directory}/include -I{DMMLIB Build Directory} {DMMLIB Build Directory}/libdmm\_static.a yourapp.c -o yourapp_
* Use the dynamic linker to preload dmmlib during application's execution:  
_LD\_PRELOAD={DMMLIB Build Directory}/src/libdmm\_shared.so application_

## Contact Information

Ioannis Koutras (joko@microlab.ntua.gr)  
Alex Bartzas (alexis@microlab.ntua.gr)
