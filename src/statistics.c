/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   statistics.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Statistics function implementation.
 */

#include "statistics.h"

#include "trace.h"

/** Updates a dmmstats_t variable
 *
 * @param dmm_stats Pointer to the dmmstats_t variable.
 * @param event     The event type, could be MALLOC or FREE.
 * @param mem_alloc Allocated memory size info.
 * */
#ifdef REQUEST_SIZE_INFO
 /**
  * @param mem_req   Requested memory size info.
  */
#endif /* REQUEST_SIZE_INFO */
void update_stats
(
 dmmstats_t *dmm_stats, 
 dmm_event_type event, 
#ifdef REQUEST_SIZE_INFO
 size_t mem_req,
#endif /* REQUEST_SIZE_INFO */
 size_t mem_alloc
) {
    if(event == MALLOC) {
        dmm_stats->total_mem_allocated += mem_alloc;
#ifdef REQUEST_SIZE_INFO
        dmm_stats->total_mem_requested += mem_req;
#endif /* REQUEST_SIZE_INFO */
        dmm_stats->live_objects++;
        dmm_stats->num_malloc++;
    }

    if(event == FREE) {
        dmm_stats->total_mem_allocated -= mem_alloc;
#ifdef REQUEST_SIZE_INFO
        dmm_stats->total_mem_requested -= mem_req;
#endif /* REQUEST_SIZE_INFO */
        dmm_stats->live_objects--;
        dmm_stats->num_free++;
    }

    TRACE_2("dmmlib - ms all %zu\n", dmm_stats->total_mem_allocated);
#ifdef REQUEST_SIZE_INFO
    TRACE_2("dmmlib - ms req %zu\n", dmm_stats->total_mem_requested);
#endif /* REQUEST_SIZE_INFO */

}

