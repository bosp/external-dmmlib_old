/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   memmove.c
 * @author Ioannis Koutras
 * @date   October 2012
 * @brief  memmove() implementation
 */

#include "memmove.h"

/** Copies n bytes from string s2 to string s1. The two strings may overlap; the
 * copy is always done in a non-destructive manner.
 * 
 * Original code at:
 * http://discuss.joelonsoftware.com/default.asp?interview.11.437419.13
 */
void * memmove(void *destination, void *source, size_t count) {
    size_t i = 0;

    char *src = (char *)source;
    char *dest = (char *)destination;
    void * ptr = dest;

    char * src_beg = src;
    char * src_end = src_beg + count;

    char * dest_beg = dest;
    char * dest_end = dest_beg + count;
    if(src == dest) {
        return ptr;
    }

    if((dest_beg >= src_beg && dest_beg <= src_end) || (dest_end >= src_beg && dest_end <= src_end)) {

        //Copy from higher addresses to lower addresses
        for( i = count ; i > 0 ; i--)
        {
            *(dest + i) = *(src + i);
        }

        return ptr;
    }

    //No overlap, copy from lower addresses to higher addresses
    for( i = 0 ; i < count ; i++)
    {
        *(dest + i) = *(src + i);
    }

    return ptr;
}
