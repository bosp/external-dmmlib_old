/*
 *   Copyright 2012 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * \file   request_memory_mmap_linux.c
 * \author Ioannis Koutras
 * \date   August, 2012
 * \brief  Request additional memory space via mmap() in Linux.
 */

#include "request_memory.h"
#include <sys/stat.h> /* for open() */
#include <fcntl.h> /* for open() */

#include <sys/mman.h>

static int dev_zero_fd = -1; /* Cached file descriptor for /dev/zero. */

void *request_memory(size_t size) {	
    int fd;
    void *zone;

    if(dev_zero_fd < 0) {
        dev_zero_fd = open("/dev/zero", O_RDWR);
    }
    fd = dev_zero_fd;

    zone = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if(zone == MAP_FAILED) {
        return NULL;
    } else {
        return zone;
    }
}
