/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   src/malloc.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 *
 * @brief  Implementation of malloc() call.
 */

#include "dmmlib/dmmlib.h"

#include <inttypes.h>

#include "dmmlib/lists.h"

#include "locks.h"
#include "default_rb.h"

#ifdef WITH_ALLOCATOR_STATS
#include "statistics.h"
#endif /* WITH_ALLOCATOR_STATS */

#ifdef WITH_DEBUG
#include "debug.h"
#endif /* WITH_DEBUG */

#include "trace.h"

void * malloc(size_t size) {
    raw_block_header_t *raw_block, *new_raw_block;
    DEFAULT_RB_T *encapsulated_rb;
    void *ptr;

    ptr = NULL;

    if(size == 0) {
        return NULL;
    }
    
    /* Try to find a raw block available for allocation */

    SLIST_FOREACH(raw_block, &systemallocator.rb_head, pointers) {
        lock_raw_block(raw_block);
        encapsulated_rb = (DEFAULT_RB_T *)
            ((uintptr_t) raw_block + sizeof(raw_block_header_t));
        ptr = dmmlib_malloc(encapsulated_rb, size);
        unlock_raw_block(raw_block);

        if(ptr != NULL) {
            break;
        }
    }

    if(ptr == NULL) {

#ifdef BITMAP_RB_ONLY
        /* Check if the request would fit in a new bitmap raw block
         * FIXME currently the raw block size and resolution are fixed
         */

        size_t bm_vector_size = BMAP_EL_SIZE *
            (SYS_ALLOC_SIZE + BMAP_EL_SIZE -
                sizeof(raw_block_header_t) - sizeof(bitmap_rb_t)) /
            (BMAP_EL_SIZE + BMAP_EL_SIZE_BITS * BITMAP_RESOLUTION);

        if(2 * size > SYS_ALLOC_SIZE - sizeof(raw_block_header_t) -
                sizeof(bitmap_rb_t) - bm_vector_size) {
#endif /* BITMAP_RB_ONLY */

#ifdef FL_RB_ONLY
        if( 2 * size > SYS_ALLOC_SIZE - sizeof(raw_block_header_t) -
                sizeof(freelist_rb_t)) {
#endif /* FL_RB_ONLY */

            lock_global();
            ptr = create_raw_block(size + sizeof(raw_block_header_t),
                        BIGBLOCK);
            if(ptr != NULL) {

#ifdef WITH_DEBUG
                SLIST_INSERT_HEAD(&systemallocator.bb_head,
                        (raw_block_header_t *) ptr, pointers);
#endif /* WITH_DEBUG */

#ifdef WITH_ALLOCATOR_STATS
                update_stats(&systemallocator.dmm_stats,
                        MALLOC,
#ifdef REQUEST_SIZE_INFO
                        size,
#endif /* REQUEST_SIZE_INFO */
                        ((raw_block_header_t *)ptr)->size);
#endif /* WITH_ALLOCATOR_STATS */

                unlock_global();

                ptr = (void *)((uintptr_t) ptr + sizeof(raw_block_header_t));
            }

            unlock_global();

        } else {

            lock_global();
            new_raw_block = create_raw_block((size_t) SYS_ALLOC_SIZE,
                    DEFAULT_RB_TYPE);
            if(new_raw_block != NULL) {
                lock_raw_block(new_raw_block);
                SLIST_INSERT_HEAD(&systemallocator.rb_head, new_raw_block, pointers);
                unlock_global();

                encapsulated_rb = (DEFAULT_RB_T *)
                    ((uintptr_t) new_raw_block + sizeof(raw_block_header_t));
                ptr = dmmlib_malloc(encapsulated_rb, size);
                unlock_raw_block(new_raw_block);
            }
        }
    }

    TRACE_1("dmmlib - m %p %zu\n", ptr, size);
    
    return ptr;
}
