/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   realloc.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 *
 * @brief  Implementation of realloc() call.
 */

#include "dmmlib/dmmlib.h"

#include "locks.h"
#include "default_rb.h"
#include "other.h"

#include "release_memory.h"
#include <sys/mman.h>
#include <string.h>
#include <inttypes.h>

#ifdef WITH_DEBUG
#include "debug.h"
#endif /* WITH_DEBUG */

#include "trace.h"

void * realloc(void *ptr, size_t size) {
    raw_block_header_t *owner_raw_block;
    void *return_ptr = NULL;

    if(ptr == NULL) {
        return malloc(size);
    }

    if(size == 0) {
        free(ptr);
        goto done;
    }

    owner_raw_block = find_raw_block_owner(systemallocator.rb_head, ptr);

    if(owner_raw_block != NULL) {

        DEFAULT_RB_T *encapsulated_rb = (DEFAULT_RB_T *)
            ((uintptr_t) owner_raw_block + sizeof(raw_block_header_t));
        
        return_ptr = dmmlib_realloc(encapsulated_rb, ptr, size);
        goto done;

    } else { /* This has to be a big block */

        // The new size is checked and if it is smaller than the one from the
        // original request, the remaining memory is simply released. If it is
        // greater, a new big block is initialized, data is copied there and the
        // old big block gets de-allocated.

        owner_raw_block = (raw_block_header_t *)
            ((uintptr_t) ptr - sizeof(raw_block_header_t));

        size_t full_size = sizeof(raw_block_header_t) + size;

        if(full_size <= owner_raw_block->size) {

            size_t remaining_size = owner_raw_block->size - full_size;
            owner_raw_block->size = full_size;

            // FIXME This is mmap-specific
            munmap((void *)((uintptr_t) owner_raw_block +
                        owner_raw_block->size), remaining_size);

#ifdef WITH_ALLOCATOR_STATS
            systemallocator.dmm_stats.total_mem_allocated -=
                remaining_size;
            TRACE_2("dmmlib - ms all %zu\n",
                    systemallocator.dmm_stats.total_mem_allocated);
#ifdef REQUEST_SIZE_INFO
            systemallocator.dmm_stats.total_mem_requested -=
                remaining_size;
            TRACE_2("dmmlib - ms req %zu\n",
                    systemallocator.dmm_stats.total_mem_requested);
#endif /* REQUEST_SIZE_INFO */
            systemallocator.dmm_stats.num_realloc++;
#endif /* WITH_ALLOCATOR_STATS */

            return_ptr = ptr;
            goto done;

        } else { /* We have to create a new big block */

#ifdef WITH_ALLOCATOR_STATS
            size_t size_diff = full_size - owner_raw_block->size;
#endif /* WITH_ALLOCATOR_STATS */

            raw_block_header_t *new_block = create_raw_block(full_size,
                    BIGBLOCK);

            if(new_block == NULL) {

                return_ptr = NULL;
                goto done;

            } else {

                return_ptr = (void *)
                    ((uintptr_t) new_block + sizeof(raw_block_header_t));

                memcpy(return_ptr,
                        ptr,
                        owner_raw_block->size - sizeof(raw_block_header_t));

#ifdef WITH_DEBUG
                lock_global();
                SLIST_REMOVE(&systemallocator.bb_head, owner_raw_block,
                        raw_block_header_s, pointers);
                SLIST_INSERT_HEAD(&systemallocator.bb_head,
                        new_block, pointers);
                unlock_global();
#endif /* WITH_DEBUG */

                release_memory(owner_raw_block);

#ifdef WITH_ALLOCATOR_STATS
                lock_global();
                systemallocator.dmm_stats.total_mem_allocated +=
                    size_diff;
                TRACE_2("dmmlib - ms all %zu\n",
                        systemallocator.dmm_stats.total_mem_allocated);
#ifdef REQUEST_SIZE_INFO
                systemallocator.dmm_stats.total_mem_requested +=
                    size_diff;
                TRACE_2("dmmlib - ms req %zu\n",
                        systemallocator.dmm_stats.total_mem_requested);
#endif /* REQUEST_SIZE_INFO */
                systemallocator.dmm_stats.num_realloc++;
                unlock_global();
#endif /* WITH_ALLOCATOR_STATS */

                goto done;
            }
        }
    }

done:

    TRACE_1("dmmlib - r %p %p %zu\n", ptr, return_ptr, size);

    return return_ptr;

}
