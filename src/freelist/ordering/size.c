/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#include "freelist/ordering/size.h"
#include "freelist/block_header_funcs.h"

void add_to_size_ordered(struct fl_head_s *head, block_header_t *block) {
    block_header_t *current_block, *next_block;
    size_t block_size;

    block_size = get_size(block);

    if(head->slh_first == NULL) {
        SLIST_INSERT_HEAD(head, block, pointers);
    } else {
        SLIST_FOREACH(current_block, head, pointers) {
            next_block = current_block->pointers.sle_next;
            if(next_block != NULL) {
                if(get_size(next_block) > block_size) {
                    current_block->pointers.sle_next = block;
                    block->pointers.sle_next = next_block;
                    break;
                }
            } else { /* We are on the end of the list, add the block there */
                current_block->pointers.sle_next = block;
                block->pointers.sle_next = NULL;
                break;
            }
        }
    }
    return;
}
