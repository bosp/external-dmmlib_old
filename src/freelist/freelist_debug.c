/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   freelist_debug.c
 * @author Ioannis Koutras (joko@microlab.ntua.gr)
 * @date   September 2012
 * @brief  Debug functions implementation for freelist-organised raw blocks
 */

#include "freelist/freelist_debug.h"

#include "dmmlib/freelist/block_header.h"
#include "freelist/block_header_funcs.h"

#include "trace.h"

/** Prints the list of free blocks of a specific freelist-organised raw block.
 *
 * @param raw_block The pointer of the freelist raw block.
 */
void get_memory_blocks(freelist_rb_t *raw_block) {
    block_header_t *memory_block;
    int counter;

    counter = 1;

    SLIST_FOREACH(memory_block, &raw_block->fl_head, pointers) {
        counter++;
        TRACE_1("Free memory block at %p with size %zu\n",
                (void *)memory_block, get_size(memory_block));
    }

    TRACE_1("Raw block at %p has %d memory blocks\n",
            (void *)raw_block, counter);
}
