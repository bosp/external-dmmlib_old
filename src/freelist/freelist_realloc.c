/*
 *   Copyright 2012 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   freelist_realloc.c
 * @author Ioannis Koutras
 * @date   September, 2012
 * @brief  realloc() implementation for freelist-organized raw blocks
 */

#include "dmmlib/freelist/freelist.h"
#include "dmmlib/freelist/block_header.h"

#include <string.h>

#ifdef HAVE_LOCKS
#include "dmmlib/raw_block.h"
#include <inttypes.h>
#endif /* HAVE_LOCKS */
#include "locks.h"

/**
 * Reallocates a memory block from a freelist-organized raw block
 *
 * @param  raw_block The pointer of the freelist raw block.
 * @param  ptr       The pointer of the memory block to be re-allocated.
 * @param  req_size  The requested memory size.
 * @retval           The address to serve the request.
 * @retval NULL      No available memory space.
 */
void * freelist_realloc(freelist_rb_t *raw_block, void *ptr,
        size_t req_size) { 
    block_header_t *block;
    void *ret;

    block = (block_header_t *)((char *) ptr - HEADER_SIZE);

    ret = freelist_malloc(raw_block, req_size);

    if(ret == NULL) {
        return NULL;
    }

    memcpy(ret, ptr, block->size);

#ifdef HAVE_LOCKS
    raw_block_header_t *rb;
    rb = (raw_block_header_t *)
        ((uintptr_t) raw_block - sizeof(raw_block_header_t));
#endif /* HAVE_LOCKS */
    lock_raw_block(rb);
    freelist_free(raw_block, ptr);
    unlock_raw_block(rb);
    return ret;
}
