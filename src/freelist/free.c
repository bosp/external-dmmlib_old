/*
 *   Copyright Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

/**
 * @file   freelist/free.c
 * @author Ioannis Koutras
 * @brief  free() implementation for freelist-organised raw blocks
 */

#include "dmmlib/freelist/freelist.h"

#include "freelist/block_header_funcs.h"
#include "freelist/ordering_policy.h"
#if defined (COALESCING_FIXED) || defined (COALESCING_VARIABLE)
#include "freelist/coalesce.h"
#endif /* COALESCING_FIXED || COALESCING_VARIABLE */

#include "trace.h"

#ifdef WITH_ALLOCATOR_STATS
#include "locks.h"
#include "statistics.h"
#include "dmmlib/dmmlib.h"
#endif /* WITH_ALLOCATOR_STATS */

/** Frees the memory block inside of a specific free-list organized raw block.
 * @param raw_block The pointer of the freelist raw block.
 * @param ptr       The pointer of the memory block to be freed.
 */
void freelist_free(freelist_rb_t *raw_block, void *ptr) {

    block_header_t *block = get_header(ptr);

    // Memory stats get updated here in case the space gets coalesced with its
    // free neighbors.

#ifdef WITH_ALLOCATOR_STATS
            lock_global();
            update_stats(&systemallocator.dmm_stats,
                    FREE,
#ifdef REQUEST_SIZE_INFO
                    get_requested_size(block),
#endif /* REQUEST_SIZE_INFO */
                    get_size(block) + HEADER_SIZE);
            unlock_global();
#endif /* WITH_ALLOCATOR_STATS */

#if defined (COALESCING_FIXED) || defined (COALESCING_VARIABLE)
    coalesce(raw_block, block);
#else
    mark_free(raw_block, block);
    ADD_BLOCK(block);
#endif /* COALESCING_FIXED || COALESCING_VARIABLE */

}
