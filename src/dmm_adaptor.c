/*
 *   Copyright 2011 Institute of Communication and Computer Systems (ICCS) 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */

#include "dmm_adaptor.h"

void update_frag_params(heap_t *heap) {
    switch(heap->dmm_knobs.frag_state) {
        default :
            heap->dmm_knobs.percentage = 0;
            heap->dmm_knobs.max_coalesce_size = (size_t) -1;
            heap->dmm_knobs.min_split_size = 0;
            heap->dmm_knobs.empty_threshold = 1.5;
            break;
        case 1 :
            heap->dmm_knobs.percentage = 20;
            heap->dmm_knobs.max_coalesce_size = 400;
            heap->dmm_knobs.min_split_size = 1200;
            break;
        case 2 :
            heap->dmm_knobs.percentage = 40;
            heap->dmm_knobs.max_coalesce_size = 800;
            heap->dmm_knobs.min_split_size = 1000;
            break;
        case 3 :
            heap->dmm_knobs.percentage = 60;
            heap->dmm_knobs.max_coalesce_size = 1200;
            heap->dmm_knobs.min_split_size = 800;
            break;
        case 4 :
            heap->dmm_knobs.percentage = 80;
            heap->dmm_knobs.max_coalesce_size = 1600;
            heap->dmm_knobs.min_split_size = 600;
            break;
        case 5 :
            heap->dmm_knobs.percentage = 100;
            heap->dmm_knobs.max_coalesce_size = 2000;
            heap->dmm_knobs.min_split_size = 300;
            break;
    }
}

void update_foot_params(heap_t *heap) {
    switch(heap->dmm_knobs.foot_state) {
        default :
            heap->dmm_knobs.empty_threshold = 0.8f;
            break;
        case 2 :
            heap->dmm_knobs.empty_threshold = 0.6f;
            break;
        case 3 :
            heap->dmm_knobs.empty_threshold = 0.4f;
            break;
        case 4 :
            heap->dmm_knobs.empty_threshold = 0.2f;
            break;
        case 5 :
            heap->dmm_knobs.empty_threshold = 0.0f;
            break;
    }
}

float get_current_fragmentation(heap_t *heap) {
    float fragmentation;

    fragmentation = (float) heap->dmm_stats.mem_allocated / 
        (float)	heap->dmm_stats.mem_requested - 1.0f;

    if(fragmentation <= MIN_FRAG_THRESHOLD &&
            heap->dmm_stats.mem_allocated < heap->dmm_knobs.mem_threshold) {
        heap->dmm_knobs.frag_state = 0;
        heap->dmm_knobs.foot_state = 0;
        update_frag_params(heap);
    }

    return fragmentation;
}

void check_footprint(heap_t *heap) {
    if(heap->dmm_stats.mem_allocated > heap->dmm_knobs.mem_threshold) {
        if(heap->dmm_knobs.frag_state > heap->dmm_knobs.foot_state) {
            heap->dmm_knobs.foot_state = heap->dmm_knobs.frag_state;
        } else {
            heap->dmm_knobs.foot_state++;
        }
        update_foot_params(heap);
    }
}

void malloc_state_refresh(heap_t *heap) {
    float fragmentation;

    fragmentation = get_current_fragmentation(heap); 

    /* Check fragmentation */
    if(fragmentation >= heap->dmm_knobs.frag_threshold &&
            heap->dmm_stats.mem_requested != 0) {
        if(heap->dmm_knobs.foot_state > heap->dmm_knobs.frag_state) {
            heap->dmm_knobs.frag_state = heap->dmm_knobs.foot_state;
        } else {
            heap->dmm_knobs.frag_state++;
        }
        update_frag_params(heap);
    }

    check_footprint(heap);
}

void free_state_refresh(heap_t *heap) {
    float fragmentation;

    fragmentation = get_current_fragmentation(heap);

    check_footprint(heap);
}
